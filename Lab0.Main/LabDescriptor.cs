﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    public class LabDescriptor
    {
        public static Type A = typeof(Filmy);
        public static Type B = typeof(Horror);
        public static Type C = typeof(Komedia);

        public static string commonMethodName = "Info";
    }
}
