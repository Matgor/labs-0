﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    class Program
    {
        static void Main(string[] args)
        {
            Filmy test = new Horror("Teksańska masakra piłą mechaniczną", "Marcus Nispel", 2003, 18);
            Console.WriteLine(test.Info());
            Console.WriteLine();
            Filmy test2 = new Komedia("The Girl Next Door", "Luke Greenfield", 2004, "romantyczna");
            Console.Write(test2.Info());
            Console.ReadKey();
        }
    }
    class Filmy
    {
        public Filmy() { }
        protected string nazwa;
        protected string rezyser;
        protected int rok_produkcji;

        public Filmy(string nazwa, string rezyser, int rok_produkcji)
        {
            this.nazwa = nazwa;
            this.rezyser = rezyser;
            this.rok_produkcji = rok_produkcji;
        }
        public virtual string Info()
        {
            return "Informacje o filmie: " + Environment.NewLine + "Nazwa: " + nazwa + Environment.NewLine + "Rok produkcji: " + rok_produkcji + Environment.NewLine + "Reżyser: " + rezyser;
        }
    }
    class Horror : Filmy
    {
        public Horror() { }
        int ograniczenie;
        public Horror(string nazwa, string rezyser, int rok_produkcji, int ograniczenie)
            : base(nazwa, rezyser, rok_produkcji)
        {
            this.ograniczenie = ograniczenie;
        }

        public override string Info()
        {
            return base.Info() + Environment.NewLine + "Horror od " + ograniczenie + " lat";

        }

    }
    class Komedia : Filmy
    {
        public Komedia() { }
        public Komedia(string nazwa, string rezyser, int rok_produkcji, string typ1)
            : base(nazwa, rezyser, rok_produkcji)
        {
            typ = typ1;
        }
        public string typ
        {
            get;
            private set;
        }
        public override string Info()
        {
            return base.Info() + Environment.NewLine + "Typ: " + typ;

        }
    }
}

